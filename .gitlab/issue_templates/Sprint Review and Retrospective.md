# Sprint Review and Retrospective Meeting

At the end of a sprint, we should take the opportunity to quickly review the progress from the previous iteration and how we can improve our processes moving forward.

These meetings will be back-to-back and timeboxed to no more than 50 minutes. This may be adjusted if additional team members are added.

## Review [15 minutes]

The Review is an opportunity to share what we've accomplished or learned during the previous sprint. This is a chance to demonstrate the working product.

- [ ] Referencing the sprint goal, was this sprint successful?
- [ ] Individual discussion of notable accomplishments of previous sprint
- [ ] Demonstrate the current state of the project internally

## Retrospective [35 minutes]

[View the Retrospective board here!](https://reetro-io.herokuapp.com/board/5ec9970f5b1e550016b914e0/5ec997b35b1e550016b914e9)

### Start [10 minutes]

- [ ] What can we start doing that will add to our process?

### Stop [10 minutes]

- [ ] What inefficient or non-value actions should we stop doing?

### Continue [10 minutes]

- [ ] What are valuable things should we continue to emphasize but are not yet habits?

### Voting [5 minutes]

In our small team, this is an unnecessary step, but may be good to get in the habit of assigning emphasis democratically.

- [ ] Each team member should assign three votes to the items they feel are the highest priority

## Next Retrospective

Reference the results of the previous sprint's Retrospective to evaluate how we have adapted and where we should focus next.

![image](/uploads/f2e2b0f735d66d4f9c6db5f52efa92c2/image.png)
