# As a

[type of user]

# I want

[goal]

# So that

[reason]

# Acceptance Criteria

## Main Path

- Given (explanation of user)
- When (details of user action)
- Then (details of expected result)

# Story-Specific Risks

| Risk | Likelihood (1-5) | Impact (1-5) |
| ---- | ---------------- | ------------ |
| TBD  | 1                | 1            |

# Initial Story Setup

- [ ] Has the story been clearly identified?
- [ ] Have any dependencies been identified?
- [ ] Has the issue been tagged with the appropriate story point value and tags?
- [ ] Has the issue been assigned to a user?
- [ ] Have any risks been identified?

# Definition of Done

- [ ] Write acceptance criteria (GIVEN-WHEN-THEN)
- [ ] Write acceptance tests
- [ ] Integration tests are written
- [ ] Unit tests are written
- [ ] Code is written
- [ ] Code is documented
- [ ] Code checked into the repository
- [ ] Code is peer reviewed
- [ ] Code deployed to the test environment
- [ ] Unit tests are passed
- [ ] Integration tests passed
- [ ] Acceptance tests passed
