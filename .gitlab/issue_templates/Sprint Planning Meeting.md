The Sprint Planning Meeting will generate:

- A goal for the upcoming sprint
- User Stories for our backlog

These will be held after the previous sprint's Retrospective meeting, and ideally before the beginning of the sprint in focus.

This meeting will not be timeboxed, but is recommended to take no more than an hour, depending on the definition required.

# Scrum Master Responsibilities

These tasks should all be accomplished by the Scrum Master leading up to the meeting:

- [ ] Organize the backlog and determine priorities
- [ ] Determine velocity
  - [ ] Update sprint documentation
- [ ] Generate a time report
- [ ] Review schedules
- [ ] Create an agenda of any additional discussion points

# Meeting Agenda

- [ ] Determine the sprint goal
- [ ] Discuss bandwidth and schedules for the upcoming sprint and [update wiki](https://gitlab.com/neal.strobl/wsu-adventurers-guild/-/wikis/Home/Agile/Cadence), if needed
- [ ] Present action items from Retrospective
- [ ] Confirm and record known and new issues
- [ ] Review the time and burndown reports, present velocity
- [ ] Present the current backlog
  - [ ] Have definitions changed since issues were last created?
  - [ ] Are story estimates still valid?
  - [ ] Have any dependencies changed?
- [ ] Generate new user stories
  - [ ] Brainstorm stories: http://scrumblr.ca/AGWSU
  - [ ] Create backlog issues for each story
  - [ ] Generate acceptance criteria
  - [ ] Generate tasks
- [ ] Sign up for and estimate work
- [ ] Review sprint deliverables and wrap up

# Meeting Outputs

## Sprint Goal

What is our primary goal for the upcoming sprint? Write one or two sentences that can be used to easily measure if the sprint was successful.

## Backlog

When complete, we should have generated fully-formed User Stories for our backlog.
