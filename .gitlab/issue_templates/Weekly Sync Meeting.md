# Agenda

1.  Accomplishments from past week / Stand-up Review [5m]
    - [ ] What did we get done?
    - [ ] Any notable successes or accomplishments?
2.  Schedule updates [6m]
    - [ ] Review schedule for the current and following weeks
3.  Identify blockers [3m]
    - [ ] Highlight anything that may be obstructing someone's progress and make a plan to address
4.  KPI Review [3m]
    - [ ] How does our progress compare to our ideal line?
    - [ ] How do we feel about our time spent with relation to output? Is anyone feeling burned out, and do we need to adjust our task list?
5.  Meeting discussion topics [10m]
    - [ ] Discuss the most pressing outstanding discussions from our "Discussions to have" list
6.  Identify new discussion topics [3m]
    - [ ] These are things that we'll want to discuss later or outside of the meeting. Create new tasks in "Discussions to have"

# Guidelines

These meetings will be timeboxed at 30 minutes.
Additional follow-up discussion can take place after if there is anything pressing, but aim to resolve all discussion topics within the 30 minutes.

# Meeting Notes

Notes will be logged by either the meeting leader or an appointed transcriber.

# Daily Stand-up

Daily Stand-ups will be asynchronous and logged in the comment section for next upcoming Sync Meeting.

Stand-ups should follow the format of:

Daily Standup [Name] (yyyy mm dd)  
Yesterday:  
Today:  
{Blockers:} (optional)
