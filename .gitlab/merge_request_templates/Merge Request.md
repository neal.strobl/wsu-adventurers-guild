[ Description of what is accomplished by this merge ]

Merge Review:

- [ ] Pull down code
- [ ] Run Unit Tests
- [ ] Run Integration Tests
- [ ] Run Acceptance Tests
- [ ] Approve/Reject Merge

Test Devices:

- [ ] Chrome
- [ ] Firefox
- [ ] Edge
- [ ] Pixel 2
- [ ] iPhone 6
- [ ] iPhone X
- [ ] iPad
- [ ] Galaxy S9
- [ ] Galaxy Note 3
- [ ] Kindle Fire HDX
