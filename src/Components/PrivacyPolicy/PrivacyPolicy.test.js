/**
 * @fileoverview Tests for PrivacyPolicy.js
 */

import React from "react";
import { render } from "enzyme";

import PrivacyPolicy from "./PrivacyPolicy";

it("should render Privacy Policy", () => {
  const component = render(<PrivacyPolicy />);
  expect(component).toMatchSnapshot();
});
