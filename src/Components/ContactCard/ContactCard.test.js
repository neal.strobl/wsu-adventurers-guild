/**
 * @fileoverview Tests for ContactCard.js
 */

import React from "react";
import { render } from "enzyme";

import ContactCard from "./ContactCard";

it("should render Navigation", () => {
  let name = "Test Name";
  let title = "Test Title";
  let email = "test@test.com";
  const component = render(
    <ContactCard name={name} title={title} email={email} />
  );
  expect(component).toMatchSnapshot();
});
