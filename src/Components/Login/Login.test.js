/**
 * @fileoverview Tests for Login.js
 */

import React from "react";
import { mount } from "enzyme";

import Login from "./Login";

it("should render dialog with no error message when failed login false", () => {
  let open = true;
  let errorMessage = "test";
  let failedLogin = false;
  let handleClose = () => {};
  let handleLogin = () => {};

  const component = mount(
    <Login
      open={open}
      errorMessage={errorMessage}
      failedLogin={failedLogin}
      handleClose={handleClose}
      handleLogin={handleLogin}
    />
  );
  expect(component).toMatchSnapshot();
  component.unmount();
});

it("should render dialog with error message when failed login true", () => {
  let open = true;
  let errorMessage = "test";
  let failedLogin = true;
  let handleClose = () => {};
  let handleLogin = () => {};
  let forgotPassword = () => {};

  const component = mount(
    <Login
      open={open}
      errorMessage={errorMessage}
      failedLogin={failedLogin}
      forgotPassword={forgotPassword}
      handleClose={handleClose}
      handleLogin={handleLogin}
    />
  );
  expect(component).toMatchSnapshot();
  component.unmount();
});
