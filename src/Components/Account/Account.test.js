/**
 * @fileoverview Tests for Account.js
 */

import React from "react";
import { render } from "enzyme";

import { Provider } from "react-redux";

import store from "../../store/store";

import Account from "./Account";

it("should render Account", () => {
  const component = render(
    <Provider store={store}>
      <Account />
    </Provider>
  );
  expect(component).toMatchSnapshot();
});
