/**
 * @fileoverview React Component for Email Dialog
 */

import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  IconButton,
  Grid,
  TextField,
  withStyles,
} from "@material-ui/core";

import SendIcon from "@material-ui/icons/Send";
import DeleteIcon from "@material-ui/icons/Delete";

import styles from "./EmailDialog.jss";

const byPropKey = (propertyName, value) => {
  return { [propertyName]: value };
};

/**
 * A Dialog with an Email Form
 * @implements {Component}
 */
class EmailDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subject: "",
      body: "",
    };
  }

  render() {
    const { fullScreen, handleClose, handleSend, open, title } = this.props;
    const { subject, content } = this.state;

    return (
      <Dialog
        open={open}
        onClose={() => handleClose()}
        maxWidth="sm"
        fullWidth={true}
        fullScreen={fullScreen}
        aria-labelledby="email-dialog-title"
      >
        <DialogTitle id="email-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoFocus
                fullWidth
                id="subject"
                label="subject"
                onChange={(event) =>
                  this.setState(byPropKey("subject", event.target.value))
                }
                type="text"
                value={subject}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                autoFocus
                color="secondary"
                fullWidth
                id="content"
                label="content"
                multiline
                onChange={(event) =>
                  this.setState(byPropKey("content", event.target.value))
                }
                value={content}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <IconButton onClick={() => handleClose()}>
            <DeleteIcon color="secondary" />
          </IconButton>
          <IconButton onClick={() => handleSend(subject, content)}>
            <SendIcon color="secondary" />
          </IconButton>
        </DialogActions>
      </Dialog>
    );
  }
}

EmailDialog.propTypes = {
  open: PropTypes.bool,
  fullScreen: PropTypes.bool,
  title: PropTypes.string,
  handleClose: PropTypes.func.isRequired,
  handleSend: PropTypes.func.isRequired,
};

EmailDialog.defaultProps = {
  open: false,
  fullScreen: false,
  title: "EMAIL",
};

export default withStyles(styles)(EmailDialog);
