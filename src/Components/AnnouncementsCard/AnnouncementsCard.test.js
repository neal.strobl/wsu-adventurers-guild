/**
 * @fileoverview Tests for AnnouncementsCard.js
 */

import React from "react";
import { render } from "enzyme";

import { Provider } from "react-redux";

import store from "../../store/store";

import AnnouncementsCard from "./AnnouncementsCard";

it("should render AnnouncementsCard", () => {
  const component = render(
    <Provider store={store}>
      <AnnouncementsCard />
    </Provider>
  );
  expect(component).toMatchSnapshot();
});
