/**
 * @fileoverview Tests for Navigation.js
 */

import React from "react";
import { render } from "enzyme";

import { BrowserRouter } from "react-router-dom";

import Navigation from "./Navigation";

it("should render Navigation", () => {
  const login = () => {};
  const logout = () => {};
  const component = render(
    <BrowserRouter>
      <Navigation login={login} logout={logout} />
    </BrowserRouter>
  );
  expect(component).toMatchSnapshot();
});
