/**
 * @fileoverview React Component that displays user requests dashboard
 */

import React, { Component } from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import {
  Button,
  Grid,
  Tooltip,
  Typography,
  withStyles,
} from "@material-ui/core";

import {
  emailUser,
  emailUsers,
  banUser,
  makeAdmin,
  postSnackbarMessage,
} from "../../store/actions";
import * as selectors from "../../store/selectors";
import * as firebaseUsers from "../../firebase/firebaseUsers";

import UserManagementCard from "../UserManagementCard/UserManagementCard";
import EmailDialog from "../EmailDialog/EmailDialog";
import styles from "./UserManagementDashboard.jss";

/**
 * React Component that displays user requests dashboard
 * @implements { Component }
 */
class UserRequestsDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { emailDialogOpen: false };
  }
  componentDidMount() {
    firebaseUsers.attachUsersListener();
  }

  componentWillUnmount() {
    firebaseUsers.detachUsersListener();
  }

  openEmailDialog = () => {
    this.setState({ emailDialogOpen: true });
  };

  closeEmailDialog = () => {
    this.setState({ emailDialogOpen: false });
  };

  handleSendAllEmail = (subject, body) => {
    let emails = this.props.approvedUsers.map(
      (user) => user.emails[user.emails.length - 1]
    );
    this.props.emailUsers(emails, subject, body);
    this.closeEmailDialog();
  };

  render() {
    const {
      approvedUsers,
      banUser,
      makeAdmin,
      emailUser,
      classes,
      fullScreenDialogs,
      myUid,
    } = this.props;
    const { emailDialogOpen } = this.state;
    return (
      <React.Fragment>
        <EmailDialog
          fullScreen={fullScreenDialogs}
          open={emailDialogOpen}
          title="EMAIL ALL"
          handleClose={this.closeEmailDialog}
          handleSend={this.handleSendAllEmail}
        />
        <section>
          <Grid container spacing={2} className={classes.dashboard}>
            <Grid
              container
              item
              justifyContent="space-between"
              alignItems="center"
              xs={12}
            >
              <Typography variant="h5">User Management</Typography>
            </Grid>
            <Grid item xs={12}>
              <hr />
            </Grid>
            <Grid container item xs={12}>
              <Tooltip
                title="Emails all users of this site using Bcc. Please don't abuse this."
                aria-label="Emails all users of site using Bcc. Please don't abuse this."
              >
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  onClick={this.openEmailDialog}
                >
                  EMAIL ALL
                </Button>
              </Tooltip>
            </Grid>
            <Grid container item alignItems="center" xs={12} spacing={2}>
              {approvedUsers
                .filter((user) => user.uid !== myUid)
                .map((user, index) => {
                  return (
                    <Grid item xs={12} md={6} lg={4} key={index}>
                      <UserManagementCard
                        fullScreenDialogs={fullScreenDialogs}
                        uid={user.uid}
                        admin={user.admin}
                        displayName={
                          user.displayNames[user.displayNames.length - 1]
                        }
                        email={user.emails[user.emails.length - 1]}
                        emailUser={emailUser}
                        banUser={banUser}
                        makeAdmin={makeAdmin}
                      />
                    </Grid>
                  );
                })}
            </Grid>
          </Grid>
        </section>
      </React.Fragment>
    );
  }
}

UserRequestsDashboard.propTypes = {
  fullScreenDialogs: PropTypes.bool,
};

UserRequestsDashboard.defaultProps = {
  fullScreenDialogs: false,
};

const mapStateToProps = (state) => ({
  approvedUsers: selectors.getApprovedUsers(state),
  myUid: selectors.getUserId(state),
});

const mapDispatchToProps = (dispatch) => ({
  emailUser: bindActionCreators(emailUser, dispatch),
  emailUsers: bindActionCreators(emailUsers, dispatch),
  banUser: bindActionCreators(banUser, dispatch),
  makeAdmin: bindActionCreators(makeAdmin, dispatch),
  postSnackbarMessage: bindActionCreators(postSnackbarMessage, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(UserRequestsDashboard));
