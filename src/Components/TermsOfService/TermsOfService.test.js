/**
 * @fileoverview Tests for TermsOfService.js
 */

import React from "react";
import { render } from "enzyme";

import TermsOfService from "./TermsOfService";

it("should render Terms of Service", () => {
  const component = render(<TermsOfService />);
  expect(component).toMatchSnapshot();
});
