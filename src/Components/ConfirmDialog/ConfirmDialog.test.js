/**
 * @fileoverview Tests for ConfirmDialog.js
 */

import React from "react";
import { render } from "enzyme";

import ConfirmDialog from "./ConfirmDialog";

it("should test", () => {});

it("should render ConfirmDialog", () => {
  const handleConfirm = () => {};
  const handleCancel = () => {};
  const component = render(
    <ConfirmDialog handleConfirm={handleConfirm} handleCancel={handleCancel} />
  );
  expect(component).toMatchSnapshot();
});
