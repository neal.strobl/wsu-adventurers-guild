/**
 * @fileoverview Tests for Landing.js
 */

import React from "react";
import { render } from "enzyme";

import { BrowserRouter } from "react-router-dom";

import { Provider } from "react-redux";

import store from "../../store/store";

import Landing from "./Landing";

it("should test", () => {});

it("should render Landing Page", () => {
  const component = render(
    <BrowserRouter>
      <Provider store={store}>
        <Landing />
      </Provider>
    </BrowserRouter>
  );
  expect(component).toMatchSnapshot();
});
