/**
 * @fileoverview Tests for SignUp.js
 */

import React from "react";
import { mount } from "enzyme";

import SignUp from "./SignUp";

it("should render dialog with no error message when failed signUp false", () => {
  let open = true;
  let errorMessage = "test";
  let failedSignUp = false;
  let handleClose = () => {};
  let handleSignUp = () => {};

  const component = mount(
    <SignUp
      open={open}
      errorMessage={errorMessage}
      failedSignUp={failedSignUp}
      handleClose={handleClose}
      handleSignUp={handleSignUp}
    />
  );
  expect(component).toMatchSnapshot();
  component.unmount();
});

it("should render dialog with error message when failed signUp true", () => {
  let open = true;
  let errorMessage = "test";
  let failedSignUp = true;
  let handleClose = () => {};
  let handleSignUp = () => {};

  const component = mount(
    <SignUp
      open={open}
      errorMessage={errorMessage}
      failedSignUp={failedSignUp}
      handleClose={handleClose}
      handleSignUp={handleSignUp}
    />
  );
  expect(component).toMatchSnapshot();
  component.unmount();
});
