/**
 * @fileoverview Tests for Postcard.js
 */

import React from "react";
import { render } from "enzyme";

import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";

import Postcard from "./Postcard";

it("should render Postcard", () => {
  const cancelFunc = () => {};
  const deleteFunc = () => {};
  const expandFunc = () => {};
  const saveFunc = () => {};
  const setDirtyFunc = () => {};
  const uid = "1";

  const component = render(
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Postcard
        cancel={cancelFunc}
        delete={deleteFunc}
        expand={expandFunc}
        save={saveFunc}
        setDirty={setDirtyFunc}
        uid={uid}
      />
    </MuiPickersUtilsProvider>
  );
  expect(component).toMatchSnapshot();
});
