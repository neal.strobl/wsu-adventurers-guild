/**
 * @fileoverview React Component that displays individual user requests
 */

import React, { Component } from "react";

import PropTypes from "prop-types";

import {
  IconButton,
  Card,
  Grid,
  Tooltip,
  Typography,
  withStyles,
} from "@material-ui/core";

import EmailIcon from "@material-ui/icons/Email";
import BlockIcon from "@material-ui/icons/Block";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";

import ConfirmDialog from "../ConfirmDialog/ConfirmDialog";
import EmailDialog from "../EmailDialog/EmailDialog";
import styles from "./UserManagementCard.jss";

/**
 * React Component that displays individual user requests
 * @implements { Component }
 */
class UserRequestsCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adminConfirmOpen: false,
      banConfirmOpen: false,
      emailDialogOpen: false,
    };
  }

  confirmBanDialog = () => {
    this.setState({ banConfirmOpen: false });
    this.props.banUser(this.props.uid);
  };

  closeBanDialog = () => {
    this.setState({ banConfirmOpen: false });
  };

  openBanDialog = () => {
    this.setState({ banConfirmOpen: true });
  };

  confirmAdminDialog = () => {
    this.setState({ adminConfirmOpen: false });
    this.props.makeAdmin(this.props.uid);
  };

  closeAdminDialog = () => {
    this.setState({ adminConfirmOpen: false });
  };

  openAdminDialog = () => {
    this.setState({ adminConfirmOpen: true });
  };

  openEmailDialog = () => {
    this.setState({ emailDialogOpen: true });
  };

  closeEmailDialog = () => {
    this.setState({ emailDialogOpen: false });
  };

  sendEmail = (subject, body) => {
    this.props.emailUser(this.props.email, subject, body);
    this.setState({ emailDialogOpen: false });
  };

  render() {
    const { classes, admin, displayName, fullScreenDialogs } = this.props;
    const { adminConfirmOpen, banConfirmOpen, emailDialogOpen } = this.state;
    return (
      <React.Fragment>
        <EmailDialog
          fullScreen={fullScreenDialogs}
          open={emailDialogOpen}
          title={"EMAIL " + displayName}
          handleClose={this.closeEmailDialog}
          handleSend={this.sendEmail}
        />
        <ConfirmDialog
          cancelButtonText="Nevermind"
          confirmButtonText="Ban User"
          fullScreen={fullScreenDialogs}
          handleConfirm={this.confirmBanDialog}
          handleCancel={this.closeBanDialog}
          message="This will permanetly ban this user from the site. This can only be reversed by editing database values."
          open={banConfirmOpen}
          title={"Really Ban " + displayName + "?"}
        />
        <ConfirmDialog
          cancelButtonText="Nevermind"
          confirmButtonText="Make Admin"
          fullScreen={fullScreenDialogs}
          handleConfirm={this.confirmAdminDialog}
          handleCancel={this.closeAdminDialog}
          message={
            "This will make " +
            displayName +
            " an admin for the AGWSU site. This can only be reversed by editing database values."
          }
          open={adminConfirmOpen}
          title={"Really Upgrade " + displayName + " to Admin?"}
        />
        <Card>
          <Grid container justifyContent="space-between" alignItems="center">
            <Grid item xs>
              <Typography variant="h5" className={classes.displayName}>
                {displayName}
              </Typography>
            </Grid>
            <Grid container item justifyContent="flex-end" xs>
              {!admin && (
                <Tooltip title="Ban this user" aria-label="Ban this user">
                  <IconButton onClick={() => this.openBanDialog()}>
                    <BlockIcon color="secondary" />
                  </IconButton>
                </Tooltip>
              )}
              {!admin && (
                <Tooltip
                  title="Make this user an admin"
                  aria-label="Make this user an admin"
                >
                  <IconButton onClick={() => this.openAdminDialog()}>
                    <ArrowUpwardIcon color="secondary" />
                  </IconButton>
                </Tooltip>
              )}
              <Tooltip title="Email this user" aria-label="Email this user">
                <IconButton onClick={() => this.openEmailDialog()}>
                  <EmailIcon color="secondary" />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Card>
      </React.Fragment>
    );
  }
}

UserRequestsCard.propTypes = {
  fullScreenDialogs: PropTypes.bool,
  uid: PropTypes.string,
  admin: PropTypes.bool,
  displayName: PropTypes.string,
  email: PropTypes.string,
  banUser: PropTypes.func.isRequired,
  makeAdmin: PropTypes.func.isRequired,
  emailUser: PropTypes.func.isRequired,
};

UserRequestsCard.defaultProps = {
  fullScreenDialogs: false,
  uid: "",
  admin: false,
  displayName: "Adventure Kid",
};

export default withStyles(styles)(UserRequestsCard);
