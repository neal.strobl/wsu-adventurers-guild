/**
 * @fileoverview Tests for AnnouncementsDashboard.js
 */

import React from "react";
import { render } from "enzyme";

import { Provider } from "react-redux";

import store from "../../store/store";

import AnnouncementsDashboard from "./AnnouncementsDashboard";

it("should render AnnouncementsDashboard", () => {
  const component = render(
    <Provider store={store}>
      <AnnouncementsDashboard />
    </Provider>
  );
  expect(component).toMatchSnapshot();
});
