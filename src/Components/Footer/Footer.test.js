/**
 * @fileoverview Tests for Footer.js
 */

import React from "react";
import { render } from "enzyme";

import { BrowserRouter } from "react-router-dom";

import Footer from "./Footer";

it("should test", () => {});

it("should render Footer", () => {
  const component = render(
    <BrowserRouter>
      <Footer />
    </BrowserRouter>
  );
  expect(component).toMatchSnapshot();
});
