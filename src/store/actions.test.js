/**
 * @fileoverview Tests for Actions
 */
import * as types from "./constants/actionTypes";
import {
  loginChanged,
  announcementsChanged,
  announcementsConfigChanged,
} from "./actions";

describe("actions", () => {
  //LOGIN_CHANGED
  it("should create an action to change the user login", () => {
    const payload = true;
    const expectedAction = {
      type: types.LOGIN_CHANGED,
      payload: payload,
    };
    expect(loginChanged(payload)).toEqual(expectedAction);
  });

  //ANNOUNCEMENTS_CHANGED
  it("should create an action to change the announcements", () => {
    const payload = [
      {
        id: "5WexRRbQOXWzJ0eRWypK",
        content:
          "This is a realtime test update. Content update test 2: electric boogaloo.  Content update 3: stop doing this. Content update 4: that's enough. Content update 5: I want to get off of Mr. Edit's wild ride. Content Edit 6: The Ride Never Ends. Content Edit 7: Edit Reborn.",
        pin: true,
        lastEdited: {
          seconds: 1595275208,
          nanoseconds: 760000000,
        },
        created: {
          seconds: 1594312080,
          nanoseconds: 0,
        },
        lastEditedBy: "bBo9DUACkrQj4PMfQQgaNBMwtNP2",
        title: "Test Post Please Ignore 7: Edit Rising",
        date: {
          seconds: 1595304000,
          nanoseconds: 0,
        },
        deleted: false,
        createdBy: "oEc5PemAeiePxZgoQMp4qor0Vzp1",
      },
    ];
    const expectedAction = {
      type: types.ANNOUNCEMENTS_CHANGED,
      payload: payload,
    };
    expect(announcementsChanged(payload)).toEqual(expectedAction);
  });

  //ANNOUNCEMENTS_CONFIG_CHANGED
  it("should create an action to change the announcements config", () => {
    const payload = {
      maxLandingWeeks: 3,
    };
    const expectedAction = {
      type: types.ANNOUNCEMENTS_CONFIG_CHANGED,
      payload: payload,
    };
    expect(announcementsConfigChanged(payload)).toEqual(expectedAction);
  });
});

//login
//logout
//types.ANNOUNCEMENT_CREATE
//types.ANNOUNCEMENT_EDIT
//types.ANNOUNCEMENT_DELETE,
//types.UPDATE_MAX_LANDING_WEEKS
//types.ANNOUNCEMENTS_CONFIG_CHANGED
