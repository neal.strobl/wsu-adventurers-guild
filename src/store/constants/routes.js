export const ACCOUNT = "/account";
export const ANNOUNCEMENTS_DASHBOARD = "/announcements";
export const LANDING = "/";
export const TERMS_OF_SERVICE = "/terms-of-service";
export const PRIVACY_POLICY = "/privacy-policy";
export const USER_MANAGEMENT_DASHBOARD = "/user-management";
export const USER_REQUESTS_DASHBOARD = "/user-requests";
export const VERIFICATION = "/verification";
