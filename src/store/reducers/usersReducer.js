/**
 * @fileoverview Reducer for any users actions
 */
import * as actions from "../constants/actionTypes";

const actionPending = (state, action) => ({
  ...state,
  pendingActions: state.pendingActions + 1,
});

const actionFulfilled = (state, action) => ({
  ...state,
  pendingActions: state.pendingActions - 1,
});

const actionRejected = (state, action) => ({
  ...state,
  pendingActions: state.pendingActions - 1,
  messages: [...state.messages, action.payload.message],
});

const userApproved = (state, action) => ({
  ...actionFulfilled(state, action),
  messages: [...state.messages, "User Approved"],
});

const userBanned = (state, action) => ({
  ...actionFulfilled(state, action),
  messages: [...state.messages, "User Banned"],
});

const userEmailed = (state, action) => ({
  ...actionFulfilled(state, action),
  messages: [...state.messages, "Email Sent"],
});

const usersEmailed = (state, action) => ({
  ...actionFulfilled(state, action),
  messages: [...state.messages, "Email Sent to All Users"],
});

const userMadeAdmin = (state, action) => ({
  ...actionFulfilled(state, action),
  messages: [...state.messages, "User is now an Admin"],
});

const userRejected = (state, action) => ({
  ...actionFulfilled(state, action),
  messages: [...state.messages, "User Rejected"],
});

const usersChanged = (state, action) => ({
  ...state,
  users: action.payload,
});

let handlers = {};
handlers[actions.USERS_CHANGED] = usersChanged;
handlers[actions.pendingAction(actions.APPROVE_USER)] = actionPending;
handlers[actions.pendingAction(actions.BAN_USER)] = actionPending;
handlers[actions.pendingAction(actions.MAKE_ADMIN)] = actionPending;
handlers[actions.pendingAction(actions.EMAIL_USER)] = actionPending;
handlers[actions.pendingAction(actions.EMAIL_USERS)] = actionPending;
handlers[actions.pendingAction(actions.REJECT_USER)] = actionPending;
handlers[actions.fulfilledAction(actions.APPROVE_USER)] = userApproved;
handlers[actions.fulfilledAction(actions.BAN_USER)] = userBanned;
handlers[actions.fulfilledAction(actions.MAKE_ADMIN)] = userMadeAdmin;
handlers[actions.fulfilledAction(actions.EMAIL_USER)] = userEmailed;
handlers[actions.fulfilledAction(actions.EMAIL_USERS)] = usersEmailed;
handlers[actions.fulfilledAction(actions.REJECT_USER)] = userRejected;
handlers[actions.rejectedAction(actions.APPROVE_USER)] = actionRejected;
handlers[actions.rejectedAction(actions.BAN_USER)] = actionRejected;
handlers[actions.rejectedAction(actions.MAKE_ADMIN)] = actionRejected;
handlers[actions.rejectedAction(actions.EMAIL_USER)] = actionRejected;
handlers[actions.rejectedAction(actions.EMAIL_USERS)] = actionRejected;
handlers[actions.rejectedAction(actions.REJECT_USER)] = actionRejected;
const usersReducer = (state, action) =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

export default usersReducer;
