/**
 * @fileoverview Tests for Reducers
 */
import { ActionType } from "redux-promise-middleware";

import * as actions from "../constants/actionTypes";
import { initialState } from "./reducer";
import reducer from "./reducer";

describe("reducer", () => {
  //initial
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  /** ANNOUNCEMENT TESTS **/

  it("should handle ANNOUNCEMENTS_CHANGED", () => {
    expect(
      reducer(undefined, {
        type: "ANNOUNCEMENTS_CHANGED",
        payload: [
          {
            id: "5WexRRbQOXWzJ0eRWypK",
            content:
              "This is a realtime test update. Content update test 2: electric boogaloo.  Content update 3: stop doing this. Content update 4: that's enough. Content update 5: I want to get off of Mr. Edit's wild ride. Content Edit 6: The Ride Never Ends. Content Edit 7: Edit Reborn.",
            pin: true,
            lastEdited: {
              seconds: 1595275208,
              nanoseconds: 760000000,
            },
            created: {
              seconds: 1594312080,
              nanoseconds: 0,
            },
            lastEditedBy: "bBo9DUACkrQj4PMfQQgaNBMwtNP2",
            title: "Test Post Please Ignore 7: Edit Rising",
            date: {
              seconds: 1595304000,
              nanoseconds: 0,
            },
            deleted: false,
            createdBy: "oEc5PemAeiePxZgoQMp4qor0Vzp1",
          },
        ],
      })
    ).toEqual({
      ...initialState,
      announcements: {
        ...initialState.announcements,
        announcements: [
          {
            id: "5WexRRbQOXWzJ0eRWypK",
            content:
              "This is a realtime test update. Content update test 2: electric boogaloo.  Content update 3: stop doing this. Content update 4: that's enough. Content update 5: I want to get off of Mr. Edit's wild ride. Content Edit 6: The Ride Never Ends. Content Edit 7: Edit Reborn.",
            pin: true,
            lastEdited: {
              seconds: 1595275208,
              nanoseconds: 760000000,
            },
            created: {
              seconds: 1594312080,
              nanoseconds: 0,
            },
            lastEditedBy: "bBo9DUACkrQj4PMfQQgaNBMwtNP2",
            title: "Test Post Please Ignore 7: Edit Rising",
            date: {
              seconds: 1595304000,
              nanoseconds: 0,
            },
            deleted: false,
            createdBy: "oEc5PemAeiePxZgoQMp4qor0Vzp1",
          },
        ],
      },
    });
  });

  it("should handle ANNOUNCEMENTS_CONFIG_CHANGED", () => {
    expect(
      reducer(undefined, {
        type: "ANNOUNCEMENTS_CONFIG_CHANGED",
        payload: {
          maxLandingWeeks: 3,
        },
      })
    ).toEqual({
      ...initialState,
      announcements: {
        ...initialState.announcements,
        announcementsConfig: {
          ...initialState.announcements.announcementsConfig,
          maxLandingWeeks: 3,
        },
      },
    });
  });

  //ANNOUNCEMENT_CREATE TESTS
  it("should handle ANNOUNCEMENT_CREATE_PENDING", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_CREATE.concat(
          "_".concat(ActionType.Pending)
        ),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: true },
    });
  });

  it("should handle ANNOUNCEMENT_CREATE_FULFILLED", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_CREATE.concat(
          "_".concat(ActionType.Fulfilled)
        ),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: false },
    });
  });

  it("should handle ANNOUNCEMENT_CREATE_REJECTED", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_CREATE.concat(
          "_".concat(ActionType.Rejected)
        ),
        payload: { errorCode: 400, message: "test error message" },
      })
    ).toEqual({
      ...initialState,
      announcements: {
        ...initialState.announcements,
        pendingAction: false,
        errors: ["test error message"],
      },
    });
  });

  //ANNOUNCEMENT_DELETE TESTS
  it("should handle ANNOUNCEMENT_DELETE_PENDING", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_DELETE.concat(
          "_".concat(ActionType.Pending)
        ),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: true },
    });
  });

  it("should handle ANNOUNCEMENT_DELETE_FULFILLED", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_DELETE.concat(
          "_".concat(ActionType.Fulfilled)
        ),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: false },
    });
  });

  it("should handle ANNOUNCEMENT_DELETE_REJECTED", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_DELETE.concat(
          "_".concat(ActionType.Rejected)
        ),
        payload: { errorCode: 400, message: "test error message" },
      })
    ).toEqual({
      ...initialState,
      announcements: {
        ...initialState.announcements,
        pendingAction: false,
        errors: ["test error message"],
      },
    });
  });

  //ANNOUNCEMENT_EDIT TESTS
  it("should handle ANNOUNCEMENT_EDIT_PENDING", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_EDIT.concat("_".concat(ActionType.Pending)),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: true },
    });
  });

  it("should handle ANNOUNCEMENT_EDIT_FULFILLED", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_EDIT.concat(
          "_".concat(ActionType.Fulfilled)
        ),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: false },
    });
  });

  it("should handle ANNOUNCEMENT_EDIT_REJECTED", () => {
    expect(
      reducer(undefined, {
        type: actions.ANNOUNCEMENT_EDIT.concat("_".concat(ActionType.Rejected)),
        payload: { errorCode: 400, message: "test error message" },
      })
    ).toEqual({
      ...initialState,
      announcements: {
        ...initialState.announcements,
        pendingAction: false,
        errors: ["test error message"],
      },
    });
  });

  //UPDATE_MAX_LANDING_WEEKS TESTS
  it("should handle UPDATE_MAX_LANDING_WEEKS_PENDING", () => {
    expect(
      reducer(undefined, {
        type: actions.UPDATE_MAX_LANDING_WEEKS.concat(
          "_".concat(ActionType.Pending)
        ),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: true },
    });
  });

  it("should handle UPDATE_MAX_LANDING_WEEKS_FULFILLED", () => {
    expect(
      reducer(undefined, {
        type: actions.UPDATE_MAX_LANDING_WEEKS.concat(
          "_".concat(ActionType.Fulfilled)
        ),
      })
    ).toEqual({
      ...initialState,
      announcements: { ...initialState.announcements, pendingAction: false },
    });
  });

  it("should handle UPDATE_MAX_LANDING_WEEKS_REJECTED", () => {
    expect(
      reducer(undefined, {
        type: actions.UPDATE_MAX_LANDING_WEEKS.concat(
          "_".concat(ActionType.Rejected)
        ),
        payload: { errorCode: 400, message: "test error message" },
      })
    ).toEqual({
      ...initialState,
      announcements: {
        ...initialState.announcements,
        pendingAction: false,
        errors: ["test error message"],
      },
    });
  });

  /** AUTHENTICATION TESTS **/

  // LOGIN TESTS
  it("should handle LOGIN_PENDING", () => {
    expect(
      reducer(undefined, {
        type: actions.LOGIN.concat("_".concat(ActionType.Pending)),
      })
    ).toEqual({
      ...initialState,
      user: { ...initialState.user, pendingAction: true },
    });
  });

  it("should handle LOGIN_FULFILLED", () => {
    expect(
      reducer(undefined, {
        type: actions.LOGIN.concat("_".concat(ActionType.Fulfilled)),
      })
    ).toEqual({
      ...initialState,
      user: { ...initialState.user, pendingAction: false },
    });
  });

  it("should handle LOGIN_REJECTED", () => {
    expect(
      reducer(undefined, {
        type: actions.LOGIN.concat("_".concat(ActionType.Rejected)),
        payload: { errorCode: 400, message: "test error message" },
      })
    ).toEqual({
      ...initialState,
      user: {
        ...initialState.user,
        pendingAction: false,
        failedLogin: true,
        errors: ["test error message"],
      },
    });
  });

  // LOGIN_CHANGED TEST
  it("should handle LOGIN_CHANGED", () => {
    expect(
      reducer(undefined, {
        type: actions.LOGIN_CHANGED,
        payload: true,
      })
    ).toEqual({
      ...initialState,
      user: {
        ...initialState.user,
        loggedIn: true,
        failedLogin: false,
        pendingAction: false,
      },
    });
  });

  // LOGOUT TESTS
  it("should handle LOGOUT_PENDING", () => {
    expect(
      reducer(undefined, {
        type: actions.LOGOUT.concat("_".concat(ActionType.Pending)),
      })
    ).toEqual({
      ...initialState,
      user: { ...initialState.user, pendingAction: true },
    });
  });

  it("should handle LOGOUT_FULFILLED", () => {
    expect(
      reducer(undefined, {
        type: actions.LOGOUT.concat("_".concat(ActionType.Fulfilled)),
      })
    ).toEqual({
      ...initialState,
      user: { ...initialState.user, pendingAction: false },
    });
  });

  it("should handle LOGOUT_REJECTED", () => {
    expect(
      reducer(undefined, {
        type: actions.LOGOUT.concat("_".concat(ActionType.Rejected)),
        payload: { errorCode: 400, message: "test error message" },
      })
    ).toEqual({
      ...initialState,
      user: {
        ...initialState.user,
        pendingAction: false,
        errors: ["test error message"],
      },
    });
  });
});
